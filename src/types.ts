export interface Language {
  name: string;
  proficiency: string;
}

export type Languages = Language[];