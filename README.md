# Astro CV Page

This is a project to create a CV page in [Astro](https://astro.build), even if it is a static page the information is all in a json file to be able to separate it from the presentation.
The main goal is to always have the CV ready to print or export to PDF, while at the same time be able to publish it online or to update it locally.

## Features

- Separation of information and presentation.
- Layout composed by components.
- Style using TailwindCSS.
